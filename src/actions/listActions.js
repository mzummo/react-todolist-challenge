// actions for the todo list can go here...

//
// RETRIEVE_LIST_ITEMS
//
// ADD_LIST_ITEM
//
// EDIT_LIST_ITEM
//
// DELETE_LIST_ITEM

export const RETRIEVE_LIST_ITEMS = 'RETRIEVE_LIST_ITEMS';
export const SET_LIST_ITEMS = 'SET_LIST_ITEMS';
export const ADD_LIST_ITEM = 'ADD_LIST_ITEM';
export const REMOVE_LIST_ITEM = 'REMOVE_LIST_ITEM';
export const EDIT_LIST_ITEM = 'EDIT_LIST_ITEM';
export const SET_LIST_ITEM = 'SET_LIST_ITEM';

export function retrieveListItems() {
    console.log('retrieve list items')
    return function (dispatch) {
        console.log('retrieve list items2')
        fetch('http://localhost:3001/todos')
            .then(function(response) {
                if (response.ok) {
                    response.json().then((data) => {
                        console.log('setting data')
                        console.log(data);
                        return dispatch(setListItems(data))
                    });
                } else {
                    console.log('error')
                }
            })
            .then(function(response) {

            })
            .catch(error => { console.log(error) })
        ;
    }
    // return {
    //     type: RETRIEVE_LIST_ITEMS
    // //     payload:
    // }
}


export function setListItems(items) {
    return {
        type: SET_LIST_ITEMS,
        payload: items
    };
}

export function pushListItem() {

}

export function removeListItem(item) {
 return {
         type: REMOVE_LIST_ITEM,
         payload: item
    }
}

export function deleteListItem(item) {
    return function(dispatch, getState) {
        fetch('http://localhost:3001/todos/' + item.id, {method: 'DELETE'})
            .then((response) => {
                console.log(response)
                if (!response.ok) {
                    return dispatch(setListItem(item));
                }
                // response.json().then(data => { console.log('(deleteListItem) data:'); console.log(data); })
                return dispatch(removeListItem(item))
            }).catch(error => {
                return dispatch(setListItem(item));
            })
    }
}

export function setListItem(item, index) {
    return {
        type: SET_LIST_ITEM,
        payload: item,
        index: index
    }
}

export function addItem(item) {
    return {
        type: "ADD_LIST_ITEM",
        payload: item,
    }
}
export function addListItem() {
    console.log('abc');
    // console.log(item);
    //var testRowIndex = gridData.push(TestRow) - 1;
    //this.setState((state) => ({...state, todos: [...state.todos, {title:'xxxxxxx'}]}))

    return (dispatch, getState) => {
        let state = getState();
        let title = state.list['add-item-to-list']
        let newItem = {
            index: state.list.todos.length - 1,
            title: title
        }
        console.log (newItem);
        console.log(dispatch(addItem(newItem)));
        console.log('new item')

        console.log('xxxxxxxxxxxxxxxx');
        //this.setState(state => console.log(state));
        fetch('http://localhost:3001/todos', {
            method: "POST",
            body: JSON.stringify(newItem),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            if (!response.ok) {
                removeItem(newItem)
            }
            console.log('success');
        })
        .then(response => console.log('Success:', JSON.stringify(response)))
        .catch(error => {
            console.error('Error:', error)
        });

        // return {
        //     type: ADD_LIST_ITEM,
        //     payload: newItem
        // }
    }
}

export function removeItem(item) {
    return {
        type: "REMOVE_LIST_ITEM",
        payload: item
    }
}

export function addItemToListInputChange(value) {
    return {
        type: 'ADD_ITEM_TO_LIST_INPUT',
        payload: value

    }
}

export function updateListItem(item) {

}

export function editListItem(index, value) {
    return {
        type: EDIT_LIST_ITEM,
        payload: value,
        index: index,
    }
}

export function setItemIsEditingStatus(index, status = true) {
    console.log('we are hererererere');
    return {
        type: "SET_ITEM_IS_EDITING",
        payload: status,
        index: index
    }
}

// export funtion
// export function addTodo(text) {
//     return { type: ADD_TODO, text }
// }
//
// export function toggleTodo(index) {
//     return { type: TOGGLE_TODO, index }
// }
//
// export function setVisibilityFilter(filter) {
//     return { type: SET_VISIBILITY_FILTER, filter }
// }