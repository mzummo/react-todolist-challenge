import React, { Component } from "react";
import { connect } from  'react-redux'
import { bindActionCreators } from 'redux'

import './index.css';
import Button from '../button';
import { editListItem, deleteListItem, setItemIsEditingStatus } from '../../actions/listActions'
import Input from "../input";

class List extends Component {
    constructor(props) {
        super(props)
        // this.editButton = React.createRef();
        // (could change button name and only edit 1 at a time)
        // this.editListItem = this.editListItem.bind(this);
        this.editItemInputOnChange = this.editItemInputOnChange.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleCancelEditClick = this.handleCancelEditClick.bind(this);
    }
    editItemInputOnChange(value, event) {

        console.log(value);
        console.log(event);
        console.log('bob')

    }
    // editListItem(index) {
    //     console.log(index)
    //     this.props.setItemIsEditing(index);
    //     // this.props.editListItem(item, index)
    // }
    handleEditClick(item, index) {
        this.props.setItemIsEditingStatus(index, true);
    }
    handleSaveClick(index) {
        this.props.saveUpdatedItem(index)
    }
    handleCancelEditClick(index) {
        console.log('cancel pressed');
        this.props.setItemIsEditingStatus(index, false);
    }
    createListItems() {
        // do event dispatch make this a dumb component ex: @change
        //if (this.props.todos) {
        const { todos } = this.props;
        console.log('create list items!!!!!!!!!!!!!!!!!!!!!!!!!1')
            return todos.map((item, index) => {
                return (
                    <tr key={index}>
                        <td>
                            {item.isEditing ?
                                <div>
                                    <Input
                                        name={ "editItemInput-" + index }
                                        value={item.title}
                                        onChange={(value, event) => this.editItemInputInputChange(value,event)}
                                    />
                                    <Button type='cancel' onClick={() => this.handleCancelEditClick(index)}>
                                    Cancel
                                    </Button>
                                </div>
                            :
                                item.title
                            }
                        </td>
                        <td>
                            {!item.isEditing ?
                                <Button type='edit' onClick={(event) => this.handleEditClick(item, index, event)}>
                                    {/*{ item.isEditing ? 'save' : 'edit' }*/}
                                    Edit
                                </Button>
                                :
                                <Button type='edit' onClick={(event) => this.handleSaveClick(item, index)}>
                                    Save
                                </Button>
                            }
                            <Button type='delete' onClick={() => this.props.deleteListItem(item)}>
                                Delete
                            </Button>
                        </td>
                    </tr>
                );
            })
        //}
    }
    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th width='66%'>Item</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Item</td>
                        <td>
                            <Button type='edit' onClick={''}>
                                Edit
                            </Button>
                            <Button type='delete' onClick={''}>
                                Delete
                            </Button> 
                        </td>
                    </tr>
                    { this.createListItems() }
                </tbody>
            </table>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        editListItem: editListItem,
        deleteListItem: deleteListItem,
        setItemIsEditingStatus: setItemIsEditingStatus
    }, dispatch)
}


export default connect(null, mapDispatchToProps)(List);
