// use this reducer for your todo list reducer...

import { RETRIEVE_LIST_ITEMS } from '../actions'
//TODO finish import of actions and use const

const initialState = {
    todos: [],
    'add-item-to-list': ''
}
   
const ListReducer = (state = initialState, action) => { // maybe reset back to back to { type, payload }
    console.log('reducer')
    console.log(action)
    let todos = state.todos;

    switch (action.type) {
        case "SET_LIST_ITEMS":
            let items = action.payload;
            return { ...state, todos:  action.payload }
        case "ADD_LIST_ITEM":
            let item = action.payload;
            return { ...state, todos: [...state.todos, action.payload]};  // maybe place the index in there so we dont need logic (index 'length -1 1' and id)
        // case "SET_LIST_ITEM":
        //     let todos = state.todos;
        //     if (action.index) {
        //          todos.slice(action.index, 0, action.payload) // maybe place the index in there so we dont need logic (index 'length -1 1' and id)
        //     } else {
        //         todos.push(action.payload)
        //     }
        //
        //     [].concat(todos)
        //     console.log(todos);
        //     // sort order (using index ... should be using date for asc/desc sort as to not keep track of index)
        //      return { ...state, todos: todos}
        case "ADD_ITEM_TO_LIST_INPUT":
            return { ...state, todos:state.todos, "add-item-to-list": action.payload } // change key to ..-InputValue
        case "REMOVE_LIST_ITEM":

            // myArray = myArray.filter(function( obj ) {
            //     return obj.field !== 'some-value';
            // });
            todos.splice(action.payload.index, 1); // might want to filter or indexOf

            return { ...state, todos: todos}
        case "RE_ADD_LIST_ITEM":
            // inject into proper previous index
        case "EDIT_LIST_ITEM":
            //let todos = state.todos;
            let index = todos.indexOf(action.payload)
            todos[index] = {
                title: action.payload
            }
            // either pass in index or get it from the object?
            return { ...state, todos: todos}
        case "SET_ITEM_IS_EDITING":
            // let todo = todos[action.index];
            // todo.isEditing = true;
            //todos.splice(action.index, 1, todo);
            //todos = todos.map(function(item, index) { return index == action.index ? todo : item; });
            let newTodos = [...todos];
            newTodos[action.index].isEditing = action.payload;
            console.log('nnbnbnnb');
            console.log(newTodos);
            return { ...state, todos: newTodos }
        default:
            return state;
    }
}

export default ListReducer;
