import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators} from 'redux'
import './App.css';

import List from './components/list';
import Input from './components/input';
import Button from './components/button';

import { addListItem, retrieveListItems, addItemToListInputChange } from './actions/listActions'
// import mapStateToProps from "react-redux/es/connect/mapStateToProps";

export class App extends Component {

    constructor(props){
        super(props);
        this.addItemToListInputChange = this.addItemToListInputChange.bind(this);
    }
    render() {
        const { todos, addListItem } = this.props
        return (
            <div className="container">

                <h1>Todo List</h1>
                
                <div className='add-item-to-list'>
                    <Input
                        name='item'
                        placeholder='New Item...'
                        onChange={this.addItemToListInputChange}
                    />
                    <Button onClick={() => addListItem() } type='add'>
                      Add
                    </Button>

                </div>
                <List todos={ todos }/>
            </div>
        );
    }

    componentDidMount() {
        this.props.retrieveListItems()
    }

    addItemToListInputChange(value) {
        this.props.addItemToListInputChange(value)
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addListItem: addListItem,
        retrieveListItems: retrieveListItems,
        addItemToListInputChange: addItemToListInputChange
    }, dispatch)
}

function mapStateToProps(state) {
    return { todos: state.list.todos };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
